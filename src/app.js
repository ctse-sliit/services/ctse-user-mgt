'use strict';

import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import 'dotenv/config';
import userRouter from './routes/user.routes';
import sellerRouter from './routes/seller.routes';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  app.use(require('morgan')('dev'));
}

app.use(helmet());
app.use(express.static(__dirname + '/src/public'));

/* Routes */

// This context path unique to this service
const contextPath = "/api/user-mgt-service"

app.get(`${contextPath}/health`, (req, res) => {
  const data = {
    uptime: process.uptime(),
    message: 'Ok',
    date: new Date()
  }

  res.status(200).send(data);
});

app.use(`${contextPath}/buyer`, userRouter);
app.use(`${contextPath}/seller`, sellerRouter);

export {app, contextPath};
