import axios from "axios";
import { getConfig } from '../utils/k8s.service.discovery';

const pathResolver = getConfig();

export const createSeller = async (req, res) => {
    const {brandName, refAddress, mobileNumber, referenceItems} = req.body;
    try {
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/seller`, {brandName, refAddress, mobileNumber, referenceItems});
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
};

export const getSeller = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/seller/${id}`);
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}

export const addAddressToSeller = async (req, res) => {
    const id = req.params.id;
    const {refAddresses} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/seller/${id}`, {refAddresses});
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}

export const updateSeller = async (req, res) => {
    const id = req.params.id;
    const {brandName, refAddress, mobileNumber, referenceItems} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/seller/${id}`, {brandName, refAddress, mobileNumber, referenceItems});
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
};

export const deleteSeller = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/seller/${id}`);
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}