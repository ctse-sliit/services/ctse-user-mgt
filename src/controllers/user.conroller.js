import axios from "axios";
import { getConfig } from '../utils/k8s.service.discovery';

const pathResolver = getConfig();

export const createUser = async (req, res) => {
    const {useRef} = req.body;
    try {
        
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/buyer`, {useRef});
        res.status(200).json(response.data);   
    }
    catch (error) {
        res.status(500).json({error});
    }
}

export const getUser = async (req, res) => {
    const id = req.params.id;
    try {
       
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/buyer/${id}`);
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}

export const addAddressToUser = async (req, res) => {
    const id = req.params.id;
    const {addressId} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/buyer/addAddress/${id}`, {addressId});
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}

export const deleteUser = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/buyer/${id}`);
        res.status(200).json(response.data);
    }
    catch (error) {
        res.status(500).json({error});
    }
}