import { Router } from "express";
import * as SellerController from "../controllers/seller.controller";

const router = Router();

router.post("/", SellerController.createSeller);
router.get("/:id", SellerController.getSeller);
router.put("/:id", SellerController.updateSeller);
router.put("/address/:id", SellerController.addAddressToSeller);
router.delete("/:id", SellerController.deleteSeller);

export default router;