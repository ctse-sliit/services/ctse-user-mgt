import { Router } from "express";
import * as UserController from "../controllers/user.conroller";

const router = Router();

router.post("/", UserController.createUser);
router.get("/:id", UserController.getUser);
router.put("/:id", UserController.addAddressToUser);
router.delete("/:id", UserController.deleteUser);

export default router;